package com.humanbooster.businesscase.api.Validator;

import com.humanbooster.businesscase.api.annotation.ValueOfCountry;
import com.humanbooster.businesscase.api.enums.Country;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ValueOfCountryValidator implements ConstraintValidator<ValueOfCountry, CharSequence> {
    private List<String> acceptedValues;


    @Override
    public void initialize(ValueOfCountry annotation) {
        acceptedValues = Stream.of(annotation.enumClass().getEnumConstants())
                .map(Country::getUrl)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        return acceptedValues.contains(value.toString());
    }
}
