package com.humanbooster.businesscase.api.controller;

import com.humanbooster.businesscase.api.models.Command;
import com.humanbooster.businesscase.api.models.User;
import com.humanbooster.businesscase.api.services.BookingService;
import com.humanbooster.businesscase.api.services.CommandService;
import com.humanbooster.businesscase.api.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RequestMapping("api/user")
@RestController
@Tag(name = "User")
@CrossOrigin
@SecurityRequirement(name = "Bearer Authentication")
public class UserController {
    @Autowired
    UserService userService;

    @Autowired
    CommandService commandService;

    @Autowired
    BookingService bookingService;

    @GetMapping(value = "", produces = "application/json")
    @Operation(summary = "Récupère tous les utilisateurs")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateurs trouvé", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "401", description = "Authentification requise", content = @Content),
            @ApiResponse(responseCode = "403", description = "Vous n'êtes pas autorisé à utiliser cette requête", content = @Content),
            @ApiResponse(responseCode = "404", description = "Aucun utilisateurs trouvé", content = @Content)
    })
    public List<User> getAll(){
        List<User> users = userService.findAll();

        if (users.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Aucun utilisateurs trouvé");
        }

        return users;
    }

    @GetMapping(value = "/{user}")
    @Operation(summary = "Récupère un utilisateur en fonction de son id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur trouvé", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "401", description = "Authentification requise", content = @Content),
            @ApiResponse(responseCode = "403", description = "Vous n'êtes pas autorisé à utiliser cette requête", content = @Content),
            @ApiResponse(responseCode = "404", description = "Utilisateur introuvable", content = @Content)
    })
    public User getOne(@PathVariable(name = "user", required = false) @Parameter(name = "user", description = "User id", example = "1", schema = @Schema(implementation = Long.class)) User user){
        if(user == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Utilisateur introuvable");
        }

        return user;
    }

    @PostMapping(value = "", consumes = "application/json", produces = "application/json")
    @Operation(summary = "Ajoute un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Utilisateur ajouté", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "400", description = "Les données sont invalides", content = @Content),
            @ApiResponse(responseCode = "401", description = "Authentification requise", content = @Content),
            @ApiResponse(responseCode = "403", description = "Vous n'êtes pas autorisé à utiliser cette requête", content = @Content)
    })
    @io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
            @ExampleObject(
                    name = "Utilisateur",
                    summary = "Utilisateur",
                    value = "{\n" +
                            "  \"country\": \"France\",\n" +
                            "  \"email\": \"unique@email.com\",\n" +
                            "  \"firstname\": \"Michel\",\n" +
                            "  \"lastname\": \"Dupond\",\n" +
                            "  \"password\": \"Azerty1234\",\n" +
                            "  \"roles\": [\n" +
                            "    {\n" +
                            "      \"id\": 1\n" +
                            "    }\n" +
                            "  ]\n" +
                            "}"
            )
    }))
    public ResponseEntity<User> post(@Valid @org.springframework.web.bind.annotation.RequestBody User user){
        if(userService.findByEmail(user.getEmail()) != null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Un utilisateur avec cet email exist déja");
        }

        user = userService.save(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{user}", consumes = "application/json", produces = "application/json")
    @Operation(summary = "Modifie un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur modifié", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "400", description = "Les données sont invalides", content = @Content),
            @ApiResponse(responseCode = "401", description = "Authentification requise", content = @Content),
            @ApiResponse(responseCode = "403", description = "Vous n'êtes pas autorisé à utiliser cette requête", content = @Content),
            @ApiResponse(responseCode = "404", description = "Cet utilisateur n'existe pas", content = @Content)
    })
    @io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
            @ExampleObject(
                    name = "Utilisateur",
                    summary = "Utilisateur",
                    value = "{\n" +
                            "  \"country\": \"France\",\n" +
                            "  \"email\": \"unique@email.com\",\n" +
                            "  \"firstname\": \"Michel\",\n" +
                            "  \"lastname\": \"Dupond\",\n" +
                            "  \"password\": \"Azerty1234\",\n" +
                            "  \"roles\": [\n" +
                            "    {\n" +
                            "      \"id\": 1\n" +
                            "    }\n" +
                            "  ]\n" +
                            "}"
            )
    }))
    public ResponseEntity<User> put(@PathVariable(name = "user", required = false) @Parameter(name = "user", description = "User id", example = "1", schema = @Schema(implementation = Long.class)) User user, @Valid @org.springframework.web.bind.annotation.RequestBody User userUpdate){
        if(user == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User inexistant");
        }

        return userService.getUserResponseEntity(userUpdate, user);
    }

    @DeleteMapping(value = "{user}")
    @Operation(summary = "Supprime un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur suprmé", content = @Content),
            @ApiResponse(responseCode = "400", description = "Supression impossible", content = @Content),
            @ApiResponse(responseCode = "401", description = "Authentification requise", content = @Content),
            @ApiResponse(responseCode = "403", description = "Vous n'êtes pas autorisé à utiliser cette requête", content = @Content),
            @ApiResponse(responseCode = "404", description = "Cet utilisateur n'existe pas", content = @Content)
    })
    void delete(@PathVariable(name = "user", required = false) @Parameter(name = "user", description = "User id", example = "1", schema = @Schema(implementation = Long.class)) User user, @RequestParam(name = "deleteAnyWay", required = false) @Parameter(name = "deleteAnyWay", description = "Suprimer l'utilisateur", schema = @Schema(implementation = Boolean.class)) Boolean deleteAnyway){
        if (deleteAnyway == null){
            deleteAnyway = false;
        }

        if(user == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User inexistant");
        }else {
            List<Command> commandList = commandService.findByUser(user);

            if(!commandList.isEmpty() && !deleteAnyway){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Impossible de suprimer cette utilisateur, il a des commandes");
            }else if(deleteAnyway ) {
                commandList.forEach(command -> {
                    bookingService.findByCommand(command).forEach(booking -> bookingService.delete(booking));
                    commandService.delete(command);
                });
            }

            userService.delete(user);
        }
    }
}
