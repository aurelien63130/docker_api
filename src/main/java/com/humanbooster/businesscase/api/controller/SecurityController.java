package com.humanbooster.businesscase.api.controller;

import com.humanbooster.businesscase.api.models.User;
import com.humanbooster.businesscase.api.models.requests.AuthRequest;
import com.humanbooster.businesscase.api.models.responses.JwtResponse;
import com.humanbooster.businesscase.api.config.JwtTokenUtils;
import com.humanbooster.businesscase.api.services.RoleService;
import com.humanbooster.businesscase.api.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/security")
@Tag(name = "Security")
@CrossOrigin
public class SecurityController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenUtils jwtTokenUtil;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @PostMapping(value = "/auth")
    @Operation(summary = "Se connecter")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Connexion réussie", content = @Content),
            @ApiResponse(responseCode = "400", description = "Les données sont invalides", content = @Content),
    })
    @io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
            @ExampleObject(
                    name = "Utilisateur",
                    summary = "Utilisateur",
                    value = "{\n" +
                            "  \"email\": \"unique@email.com\",\n" +
                            "  \"password\": \"Azerty1234\"\n" +
                            "}"
            )
    }))
    public ResponseEntity<JwtResponse> authentication(@RequestBody AuthRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());

        UserDetails userDetails = this.userDetailsService.loadUserByUsername(authenticationRequest.getEmail());

        final String token = jwtTokenUtil.generateToken(userDetails.getUsername());

        return ResponseEntity.ok(new JwtResponse(token));
    }

    @PostMapping(value = "/register", consumes = "application/json", produces = "application/json")
    @Operation(summary = "Créer un compte")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Utilisateur ajouté", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "400", description = "Les données sont invalides", content = @Content)
    })
    @io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
            @ExampleObject(
                    name = "Utilisateur",
                    summary = "Utilisateur",
                    value = "{\n" +
                            "  \"country\": \"France\",\n" +
                            "  \"email\": \"unique@email.com\",\n" +
                            "  \"firstname\": \"Michel\",\n" +
                            "  \"lastname\": \"Dupond\",\n" +
                            "  \"password\": \"Azerty1234\"\n" +
                            "}"
            )
    }))
    public ResponseEntity<User> post(@Valid @org.springframework.web.bind.annotation.RequestBody User user){
        if(userService.findByEmail(user.getEmail()) != null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Un utilisateur avec cet email exist déja");
        }

        user.setRoles(new ArrayList<>());
        user.addRole(roleService.findByName("user"));
        user = userService.save(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @GetMapping(value = "/me")
    @Operation(summary = "Récupère l'utilisateur connecté")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur trouvé", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "401", description = "Authentification requise", content = @Content)
    })
    @SecurityRequirement(name = "Bearer Authentication")
    public User getCurrentUser(){
        return userService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @PutMapping(value = "/me", consumes = "application/json", produces = "application/json")
    @Operation(summary = "Modifie vos données")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Données modifié", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "400", description = "Les données sont invalides", content = @Content),
            @ApiResponse(responseCode = "401", description = "Authentification requise", content = @Content),
            @ApiResponse(responseCode = "404", description = "Cet utilisateur n'existe pas", content = @Content)
    })
    @io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
            @ExampleObject(
                    name = "Utilisateur",
                    summary = "Utilisateur",
                    value = "{\n" +
                            "  \"country\": \"France\",\n" +
                            "  \"email\": \"unique@email.com\",\n" +
                            "  \"firstname\": \"Michel\",\n" +
                            "  \"lastname\": \"Dupond\",\n" +
                            "  \"password\": \"Azerty1234\"\n" +
                            "}"
            )
    }))
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<User> put(@Valid @RequestBody User userUpdate){
        User user = userService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());

        userUpdate.setRoles(user.getRoles());

        return userService.getUserResponseEntity(userUpdate, user);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}