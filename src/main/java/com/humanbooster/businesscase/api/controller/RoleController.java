package com.humanbooster.businesscase.api.controller;

import com.humanbooster.businesscase.api.enums.Country;
import com.humanbooster.businesscase.api.models.Command;
import com.humanbooster.businesscase.api.models.Role;
import com.humanbooster.businesscase.api.models.User;
import com.humanbooster.businesscase.api.services.RoleService;
import com.humanbooster.businesscase.api.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RequestMapping("api/role")
@RestController
@Tag(name = "Rôle")
@CrossOrigin
@SecurityRequirement(name = "Bearer Authentication")
public class RoleController {
    @Autowired
    RoleService roleService;

    @Autowired
    UserService userService;

    @GetMapping(value = "", produces = "application/json")
    @Operation(summary = "Récupère tous les roles")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rôles trouvé", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Role.class))}),
            @ApiResponse(responseCode = "401", description = "Authentification requise", content = @Content),
            @ApiResponse(responseCode = "404", description = "Aucun rôles  trouvé", content = @Content)
    })
    public List<Role> getAll(){
        List<Role> roles = roleService.findAll();

        if (roles.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Aucun rôles  trouvé");
        }

        return roles;
    }

    @GetMapping(value = "/{role}")
    @Operation(summary = "Récupère un rôle en fonction de son id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rôle trouvé", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Role.class))}),
            @ApiResponse(responseCode = "401", description = "Authentification requise", content = @Content),
            @ApiResponse(responseCode = "403", description = "Vous n'êtes pas autorisé à utiliser cette requête", content = @Content),
            @ApiResponse(responseCode = "404", description = "Rôle introuvable", content = @Content)
    })
    public Role getOne(@PathVariable(name = "role", required = false) @Parameter(name = "role", description = "Rôle id", example = "1", schema = @Schema(implementation = Long.class)) Role role){
        if(role == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Rôle introuvable");
        }

        return role;
    }

    @PostMapping(value = "", consumes = "application/json", produces = "application/json")
    @Operation(summary = "Ajoute un rôle")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Rôle ajouté", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Role.class))}),
            @ApiResponse(responseCode = "401", description = "Authentification requise", content = @Content),
            @ApiResponse(responseCode = "403", description = "Vous n'êtes pas autorisé à utiliser cette requête", content = @Content),
            @ApiResponse(responseCode = "400", description = "Les données sont invalides", content = @Content)
    })
    @io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
            @ExampleObject(
                    name = "Role",
                    summary = "Role",
                    value = "{\n" +
                            "  \"name\": \"user\"\n" +
                            "}"
            )
    }))
    public ResponseEntity<Role> post(@Valid @org.springframework.web.bind.annotation.RequestBody Role role){
        if(roleService.findByName(role.getName()) != null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Un rôle avec ce nom existe déjà");
        }

        role = roleService.saveRole(role);
        return new ResponseEntity<>(role, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{role}", consumes = "application/json", produces = "application/json")
    @Operation(summary = "Modifie un rôle")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rôle modifié", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = User.class))}),
            @ApiResponse(responseCode = "400", description = "Les données sont invalides", content = @Content),
            @ApiResponse(responseCode = "401", description = "Authentification requise", content = @Content),
            @ApiResponse(responseCode = "403", description = "Vous n'êtes pas autorisé à utiliser cette requête", content = @Content),
            @ApiResponse(responseCode = "404", description = "Rôle inexistant", content = @Content)
    })
    @io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
            @ExampleObject(
                    name = "Rôle",
                    summary = "Rôle",
                    value = "{\n" +
                            "  \"name\": \"user\"\n" +
                            "}"
            )
    }))
    public ResponseEntity<Role> put(@PathVariable(name = "role", required = false) @Parameter(name = "role", description = "Role id", example = "1", schema = @Schema(implementation = Long.class)) Role role, @Valid @org.springframework.web.bind.annotation.RequestBody Role roleUpdate){
        if(role == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Rôle inexistant");
        }
        if(!role.getName().equals( roleUpdate.getName()) && roleService.findByName(roleUpdate.getName()) != null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Un rôle avec ce nom existe déjà");
        }

        role.setName(role.getName());
        role = roleService.saveRole(roleUpdate);

        return new ResponseEntity<>(role, HttpStatus.OK);
    }

    @DeleteMapping(value = "{role}")
    @Operation(summary = "Supprime un rôle")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rôle suprmé", content = @Content),
            @ApiResponse(responseCode = "400", description = "Supression impossible", content = @Content),
            @ApiResponse(responseCode = "401", description = "Authentification requise", content = @Content),
            @ApiResponse(responseCode = "403", description = "Vous n'êtes pas autorisé à utiliser cette requête", content = @Content),
            @ApiResponse(responseCode = "404", description = "Rôle inexistant", content = @Content)
    })
    void delete(@PathVariable(name = "role", required = false) @Parameter(name = "role", description = "User id", example = "1", schema = @Schema(implementation = Long.class)) Role role){
        if(role == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Rôle inexistant");
        } else if (role.getName().equals("user") || role.getName().equals("admin")){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le rôle admin ainsi que le rôle user ne peuvent pas être supprimé");
        } else {
            userService.findByRolesContains(role).forEach(user -> user.removeRole(role));

            roleService.delete(role);
        }
    }
}
