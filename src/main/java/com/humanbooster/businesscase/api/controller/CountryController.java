package com.humanbooster.businesscase.api.controller;

import com.humanbooster.businesscase.api.enums.Country;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("api/country")
@RestController
@CrossOrigin
@Tag(name = "Country")
public class CountryController {

    @GetMapping(value = "", produces = "application/json")
    @Operation(summary = "Récupére tous les pays")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur modifié", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = String.class))}),
            @ApiResponse(responseCode = "404", description = "Aucun pays trouvé", content = @Content)
    })
    public List<String> getAll(@RequestParam(name = "search", required = false) @Parameter(name = "search", description = "Rechercher une liste de pays à partir d'un string", schema = @Schema(implementation = String.class)) String search){
        List<String> countrys = new ArrayList<>();

        if(search == null){
            for(Country country : Country.values()){
                countrys.add(country.getUrl());
            }
        }else {
            for(Country country : Country.values()){
                if(country.getUrl().toLowerCase().contains(search)) {
                    countrys.add(country.getUrl());
                }
            }
        }

        if(countrys.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Aucun pays trouvé");
        }

        return countrys;
    }
}
