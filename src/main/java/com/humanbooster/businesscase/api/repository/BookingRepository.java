package com.humanbooster.businesscase.api.repository;

import com.humanbooster.businesscase.api.models.Booking;
import com.humanbooster.businesscase.api.models.Command;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepository  extends CrudRepository<Booking, Long> {
    List<Booking> findByCommand(Command command);
}
