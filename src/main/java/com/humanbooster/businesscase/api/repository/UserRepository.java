package com.humanbooster.businesscase.api.repository;

import com.humanbooster.businesscase.api.models.Role;
import com.humanbooster.businesscase.api.models.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    @Override
    List<User> findAll();

    User findByEmail(String email);

    List<User> findByRolesContains(Role role);
}
