package com.humanbooster.businesscase.api.repository;

import com.humanbooster.businesscase.api.models.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
    @Override
    List<Role> findAll();

    Role findByName(String name);
}
