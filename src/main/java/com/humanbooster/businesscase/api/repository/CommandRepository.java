package com.humanbooster.businesscase.api.repository;

import com.humanbooster.businesscase.api.models.Command;
import com.humanbooster.businesscase.api.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommandRepository extends CrudRepository<Command, Long> {
    List<Command> findByUser(User user);
}
