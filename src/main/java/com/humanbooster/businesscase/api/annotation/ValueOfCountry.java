package com.humanbooster.businesscase.api.annotation;

import com.humanbooster.businesscase.api.Validator.ValueOfCountryValidator;
import com.humanbooster.businesscase.api.enums.Country;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ValueOfCountryValidator.class)
public @interface ValueOfCountry {
    Class<Country> enumClass() default Country.class;
    String message() default "Doit etre contenue dans {enumClass}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}