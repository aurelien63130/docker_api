package com.humanbooster.businesscase.api.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.util.Calendar;

@Entity
@Table(name = "booking")
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    @NotNull(message = "Veuillez choisir la file")
    private Integer lane;

    @Column(name = "_column")
    private Integer column;

    @Column(nullable = false)
    @NotNull(message = "Veuillez choisir la date")
    @Temporal(TemporalType.DATE)
    private Calendar date;

    @Column(nullable = false, name = "is_accepted")
    private boolean isAccepted = false;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez choisir une option")
    private String equipment;

    @ManyToOne
    private Command command;

    public Booking() {
    }

    public Booking(Integer lane, Calendar date, String equipment, Command command) {
        this.lane = lane;
        this.date = date;
        this.equipment = equipment;
        this.command = command;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getLane() {
        return lane;
    }

    public void setLane(Integer lane) {
        this.lane = lane;
    }

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }
}
