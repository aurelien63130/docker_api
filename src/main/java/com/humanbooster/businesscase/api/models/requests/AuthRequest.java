package com.humanbooster.businesscase.api.models.requests;

import io.swagger.v3.oas.annotations.media.Schema;

public class AuthRequest {
    @Schema(example = "unique@email.com")
    private String email;

    @Schema(example = "Azerty1234")
    private String password;

    public AuthRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
