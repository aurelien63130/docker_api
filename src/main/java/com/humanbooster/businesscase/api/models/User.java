package com.humanbooster.businesscase.api.models;

import com.humanbooster.businesscase.api.annotation.ValueOfCountry;
import com.humanbooster.businesscase.api.enums.Country;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(example = "1")
    private long id;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir un pay de résidence")
    @ValueOfCountry(message = "Veuiller saisir un pay valide")
    @Schema(example = "France")
    private String country;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false, name = "date_add")
    private Calendar dateAdd;

    @Column(nullable = false, unique = true)
    @Email(message = "Saisissez une adresse email valide")
    @NotBlank(message = "Veuillez saisir une adresse email")
    @Schema(example = "unique@email.com")
    private String email;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir un prénom")
    @Schema(example = "Michel")
    private String firstname;

    @Column(nullable = false)
    @NotBlank(message = "Veuillez saisir un nom de famille")
    @Schema(example = "Dupond")
    private String lastname;

    @Column(nullable = false)
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$", message = "Le mot de passe doit contenir au moins 8 caractères une majuscule une minuscule et un chiffre")
    @NotBlank(message = "Vous devez renseigner un mot de passe")
    @Schema(example = "Azerty1234")
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role")
    private List<Role> roles;

    public User() {
        roles = new ArrayList<>();
        this.dateAdd = Calendar.getInstance();
    }

    public User(String country, Calendar dateAdd, String email, String firstname, String lastname, String password) {
        this.country = country;
        this.dateAdd = dateAdd;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        roles = new ArrayList<>();
    }

    public User(String country, String email, String firstname, String lastname, String password, Role role) {
        this.country = country;
        this.dateAdd = Calendar.getInstance();
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        roles = new ArrayList<>();
        addRole(role);
    }

    public  void addRole(Role role){
        roles.add(role);
    }

    public  void removeRole(Role role){
        roles.remove(role);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Calendar getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(Calendar dateAdd) {
        this.dateAdd = dateAdd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
