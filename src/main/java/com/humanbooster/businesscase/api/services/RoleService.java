package com.humanbooster.businesscase.api.services;

import com.humanbooster.businesscase.api.models.Role;
import com.humanbooster.businesscase.api.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    public Role findByName(String name){
        return roleRepository.findByName(name);
    }

    public Role saveRole(Role role){
        return roleRepository.save(role);
    }

    public void delete(Role role){
        roleRepository.delete(role);
    }
}
