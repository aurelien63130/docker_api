package com.humanbooster.businesscase.api.services;

import com.humanbooster.businesscase.api.models.Command;
import com.humanbooster.businesscase.api.models.User;
import com.humanbooster.businesscase.api.repository.CommandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommandService {
    @Autowired
    CommandRepository commandRepository;

    public List<Command> findByUser(User user){
        return commandRepository.findByUser(user);
    }

    public void delete(Command command){
        commandRepository.delete(command);
    }
}
