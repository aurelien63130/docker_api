package com.humanbooster.businesscase.api.services;

import com.humanbooster.businesscase.api.models.Booking;
import com.humanbooster.businesscase.api.models.Command;
import com.humanbooster.businesscase.api.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookingService {
    @Autowired
    BookingRepository bookingRepository;

    public void delete(Booking booking){
        bookingRepository.delete(booking);
    }

    public List<Booking> findByCommand(Command command){
        return bookingRepository.findByCommand(command);
    }
}
