package com.humanbooster.businesscase.api.services;

import com.humanbooster.businesscase.api.models.Role;
import com.humanbooster.businesscase.api.models.User;
import com.humanbooster.businesscase.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public User save(User user){
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        return userRepository.save(user);
    }

    public User findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public List<User> findByRolesContains(Role role){
        return userRepository.findByRolesContains(role);
    }

    public void delete(User user){
        userRepository.delete(user);
    }

    public ResponseEntity<User> getUserResponseEntity(User userUpdate, User user) {
        if(!user.getEmail().equals( userUpdate.getEmail()) && userRepository.findByEmail(userUpdate.getEmail()) != null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Un utilisateur avec cet email exist déja");
        }

        userUpdate.setId(user.getId());
        userUpdate = save(userUpdate);

        return new ResponseEntity<>(userUpdate, HttpStatus.OK);
    }
}
